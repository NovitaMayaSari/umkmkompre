from flask import Flask, redirect
from dotenv import load_dotenv
from auth.auth import auth_bp
from public.public import public_bp
from user.user import user_bp
from admin.admin import admin_bp
import os

# Get environment variables
load_dotenv()

# Check Debug Status
debug_status = os.environ.get("DEBUG_STATUS")


app = Flask(__name__)

__app_name = os.environ.get("APP_NAME")
base_url = os.environ.get("BASE_URL")

app.register_blueprint(auth_bp)
app.register_blueprint(public_bp)
app.register_blueprint(user_bp)
app.register_blueprint(admin_bp)

app.config["SECRET_KEY"] = os.environ.get("SECRET_KEY")
app.config["SESSION_COOKIE_SECURE"] = os.environ.get("SESSION_COOKIE_SECURE")
    
if __name__ == "__main__":
    if debug_status == "True":
        app.run(debug=True, port=5000)
    else:
        app.run(debug=False, port=5000)