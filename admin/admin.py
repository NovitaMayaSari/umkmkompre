from flask import Blueprint, render_template, request, session, flash, redirect, url_for, jsonify, abort
from auth.check import Auth
from argon2 import PasswordHasher, Type
from jinja2 import utils
from py2neo import Graph
from dotenv import load_dotenv
import datetime
import os

# Take environment variables from .env
load_dotenv()

# inisiasi nama blueprint, peletakan folder template dan peletakan folderstatic
# template= file html
# statisc= file css dan js
admin_bp = Blueprint("admin_bp", __name__, template_folder="templates", static_folder="static")

# Set up authentication parameters
# mengambil data url, user, pass dari file .env
neo4j_url = os.environ.get("NEO4J_URL_HTTPS")
neo4j_user = os.environ.get("NEO4J_USER")
neo4j_pass = os.environ.get("NEO4J_PASSWORD")

#mengkoneksikan ke aura
graph = Graph(neo4j_url, user=neo4j_user, password=neo4j_pass)

# mengambil data app name, base url dari file.env
__app_name = os.environ.get("APP_NAME")
base_url = os.environ.get("BASE_URL")

# Untuk menampilkan halaman dashboard admin
@admin_bp.route("/admin/dashboard")
def dashboard():
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        # mempersiapkan data yang akan dikirim ke halaman dashboard
        now = datetime.datetime.now()
        # query untuk data yang ditampilkan pada dashboard
        jumlah_pengguna= graph.query("MATCH (n:users) WHERE n.level<>'admin' RETURN COUNT(n) AS jumlah_pengguna").data()[0]["jumlah_pengguna"]
        jumlah_umkm = graph.query("MATCH (n:UMKM) RETURN COUNT(n) AS jumlah_umkm").data()[0]["jumlah_umkm"]
        jumlah_produk = graph.query("MATCH (n:produk) RETURN COUNT(n) AS jumlah_produk").data()[0]["jumlah_produk"]

        return render_template(
            "admin/dashboard.html",
            title="Dashboard",
            app_name=__app_name,
            base_url= base_url,
            year=now.year,
            jumlah_pengguna=jumlah_pengguna,
            jumlah_umkm=jumlah_umkm,
            jumlah_produk=jumlah_produk
        )

# proses untuk menampilkan halaman pengguna
@admin_bp.route("/admin/user")
def user():
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        # mempersiapkan data yang akan dikirim
        now = datetime.datetime.now()
        # query untuk menampilkan data user
        data_user = graph.run("MATCH (u:users) WHERE u.level<>'admin' RETURN id(u) as id, u.nama as nama, u.email as email, u.nohp as nohp, u.level as level").data()
        return render_template(
            "admin/user.html",
            title = "Pengguna",
            app_name =__app_name,
            base_url = base_url,
            year = now.year,
            data_user = data_user
        )

# proses tambah user
@admin_bp.route("/admin/tambah_user", methods=["POST"])
def tambah_user():
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        nama = request.form.get("nama")
        email = request.form.get("email")
        nohp = request.form.get("nohp")
        level = "user"

        ph = PasswordHasher(
            memory_cost=65536,
            time_cost=4,
            parallelism=2,
            hash_len=32,
            type=Type.ID
        )
        password = ph.hash("123456")

        try:
            # query untuk tambah user melalui admin
            graph.run("CREATE (u:users {nama: '" + nama + "', email: '" + email + "', nohp: '" + nohp + "', level: '" + level + "', password: '" + password + "'})")
            flash("Data berhasil ditambahkan.", "success")
            return redirect(url_for("admin_bp.user"))
        except:
            flash("Data gagal ditambahkan.", "error")
            return redirect(url_for("admin_bp.user"))

# peoses untuk menampilkan halaman edit user
@admin_bp.route("/admin/edit_user/<id>")
def edit_user(id):
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        now = datetime.datetime.now()
         # query untuk menampilkan data user yang akan di edit
        data_user = graph.run("MATCH (u:users) WHERE id(u)="+str(id)+" RETURN id(u) as id, u.nama as nama, u.email as email, u.nohp as nohp, u.level as level").data()
        return render_template(
            "admin/user.html",
            title = "Edit Pengguna",
            app_name =__app_name,
            base_url = base_url,
            year = now.year,
            data_user = data_user
        )

# proses edit user berdasarkan submit dari halaman edit user
@admin_bp.route("/admin/proses_edit_user", methods=["POST"])
def proses_edit_user():
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        id = request.form.get("id")
        nama = request.form.get("nama")
        email = request.form.get("email")
        nohp = request.form.get("nohp")
        try:
            # query edit data user
            graph.run("MATCH (u:users) WHERE id(u)="+str(id)+" SET u.nama= '" + nama + "', u.email= '" + email + "', u.nohp= '" + nohp + "' ")
            flash("Data berhasil diedit.", "success")
            return redirect(url_for("admin_bp.user"))
        except:
            flash("Data gagal diedit.", "error")
            return redirect(url_for("admin_bp.user"))

# menamppilkan halaman edit password
@admin_bp.route("/admin/edit_password/<id>")
def edit_password(id):
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        now = datetime.datetime.now()
        return render_template(
            "admin/user.html",
            title = "Edit Pengguna",
            app_name =__app_name,
            base_url = base_url,
            year = now.year,
            data_user = id
        )

# proses edit password berdasarkan submit dari form edit password
@admin_bp.route("/admin/proses_edit_password", methods=["POST"])
def proses_edit_password():
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        id = request.form.get("id")
        ph = PasswordHasher(
            memory_cost=65536,
            time_cost=4,
            parallelism=2,
            hash_len=32,
            type=Type.ID
        )
        password = ph.hash(request.form.get("password"))
        try:
            # query untuk mengubah password pengguna
            graph.run("MATCH (u:users) WHERE id(u)="+str(id)+" SET u.password= '" + password + "' ")
            flash("Data berhasil diedit.", "success")
            return redirect(url_for("admin_bp.user"))
        except:
            flash("Data gagal diedit.", "error")
            return redirect(url_for("admin_bp.user"))

# proses hapus pengguna
@admin_bp.route("/admin/delete_user/<id>")
def delete_user(id):
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        try:
            # query untuk menghapus data pengguna dan data yang berelasi terhadap pengguna yang dihapus
            graph.run("MATCH (n) WHERE id(n)="+str(id)+" DETACH DELETE n")
            flash("Data berhasil dihapus.", "success")
            return redirect(url_for("admin_bp.user"))
        except:
            flash("Data gagal dihapus.", "error")
            return redirect(url_for("admin_bp.user"))

# Profil admin
@admin_bp.route("/ubah_profil", methods=["POST"])
def ubah_profil():
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        id = request.form.get("id")
        nama = request.form.get("nama")
        email = request.form.get("email")
        nohp = request.form.get("nohp")
        level = session.get("level")
        try:
            # query untuk mengubah data profile admin
            graph.run("MATCH (p:users) WHERE id(p)="+str(id)+" SET p.nama = '" + nama + "', p.email = '" + email + "', p.nohp = '" + nohp + "'")
            Auth.set_session(id, email, nama, level, nohp)
            flash("Data berhasil diedit.", "success")
            return redirect(url_for("admin_bp.dashboard"))
        except:
            flash("Data gagal diedit.", "error")
            return redirect(url_for("admin_bp.dashboard"))

# proses mengubah data password admin
@admin_bp.route("/ubah_password", methods=["POST"])
def ubah_password():
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        id = request.form.get("id")
        old_password = request.form.get("old_password")
        new_password = request.form.get("new_password")

        # query pencarian data id admin
        get_users = graph.run("MATCH (p:users) WHERE id(p) = " + str(id) + " RETURN n.password").data()

        ph = PasswordHasher(
            memory_cost=65536,
            time_cost=4,
            parallelism=2,
            hash_len=32,
            type=Type.ID
        )

        old_password_hash = get_users[0]["n.password"]
        new_password_hash = ph.hash(new_password)
        if ph.verify(old_password, old_password_hash):
            try:
                # query mengubah password admin
                ph.verify(password_hash, password)
                graph.run("MATCH (p:users) WHERE id(p)="+str(id)+" SET p.password = '" + new_password_hash + "'")
                flash("Password berhasil diubah.", "success")
                return redirect(url_for("admin_bp.dashboard"))
            except:
                flash("Password gagal diubah.", "error")
                return redirect(url_for("admin_bp.dashboard"))
        else:
            flash("Password lama tidak sesuai.", "error")
            return redirect(url_for("admin_bp.dashboard"))

        

def __filter_input(input, mode=True):
    if mode == True:
        input = str(utils.escape(input))

    return input
