from flask import Flask, send_from_directory, Blueprint, render_template, request, session, flash, redirect, url_for, jsonify, abort
from werkzeug.utils import secure_filename
from auth.check import Auth
from jinja2 import utils
from py2neo import Graph
from dotenv import load_dotenv
import datetime
import requests
import json
import os

# Init Flask App
app = Flask(__name__)

# Get Path
path = os.getcwd()

# set file upload location
UPLOAD_FOLDER = os.path.join(path, 'static/uploads')
app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER

# Take environment variables from .env
load_dotenv()

# inisiasi nama blueprint, peletakan folder template dan peletakan folderstatic
# template= file html
# statisc= file css dan js
user_bp = Blueprint("user_bp", __name__, template_folder="templates", static_folder="static")

# Set up authentication parameters
# mengambil data url, user, pass dari file .env
neo4j_url = os.environ.get("NEO4J_URL_HTTPS")
neo4j_user = os.environ.get("NEO4J_USER")
neo4j_pass = os.environ.get("NEO4J_PASSWORD")

#mengkoneksikan ke aura
graph = Graph(neo4j_url, user=neo4j_user, password=neo4j_pass)

# mengambil data app name, base url dari file.env
__app_name = os.environ.get("APP_NAME")
base_url = os.environ.get("BASE_URL")

# Gambar yang diperbolehkan
IMAGES_ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
def allowed_image(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in IMAGES_ALLOWED_EXTENSIONS

# File yang diperbolehkan
FILES_ALLOWED_EXTENSIONS = set(['pdf'])
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in FILES_ALLOWED_EXTENSIONS


# proses untuk menampilkan halaman dashboard user
@user_bp.route("/dashboard")
def dashboard():
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        now = datetime.datetime.now()
        # query untuk menampilkan data UMKM
        dataUMKM = graph.query("MATCH (n:users)-[]-(u:UMKM)-[]-(p:produk) WHERE n.level<>'admin' RETURN id(u) as id, n.nama as nama_pemilik, u.nama_usaha as nama_usaha, u.latitude as lat, u.longitude as long").data()
        dataTable = graph.query("MATCH (k:kategori)-[]-(u:UMKM) RETURN u.klasifikasi as klas, k.subsektor as sub, count(u) as count ORDER BY klas").data()
        return render_template(
            "user/dashboard.html",
            title="Dashboard",
            app_name=__app_name,
            base_url= base_url,
            year=now.year,
            dataUMKM = dataUMKM,
            dataTable = dataTable
        )

# proses pencarian level user
@user_bp.route("/pencarian", methods=["GET"])
def pencarian():
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        keyword = request.args.get("keyword")
        try:
            # query pencarian data UMKM 
            hasil = graph.run('MATCH (p:kategori)-[*1..2]-(q:UMKM)-[*1..2]-(r:produk) WHERE '+str(keyword)+' RETURN id(q) as id, q.nama_usaha as nama_usaha, q.latitude as lat, q.longitude as long LIMIT 50').data()
            callback = {"status": "success", "data": hasil}
        except:
            callback = {"status": "failed"}
        return jsonify(callback)

# total pencarian data
@user_bp.route("/total_pencarian", methods=["GET"])
def total_pencarian():
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        keyword = request.args.get("keyword")
        try:
            # query total data Kategori 
            queryKategori = graph.run('MATCH (p:kategori)-[]-(q:UMKM)-[]-(r:produk) WHERE '+str(keyword)+' RETURN DISTINCT p.subsektor LIMIT 50').data()
            counterKategori = 0
            for value in queryKategori:
                counterKategori += 1

            # query total data UMKM 
            queryUmkm = graph.run('MATCH (p:kategori)-[]-(q:UMKM)-[]-(r:produk) WHERE '+str(keyword)+' RETURN DISTINCT q.nama_usaha LIMIT 50').data()
            counterUmkm = 0
            for value in queryUmkm:
                counterUmkm += 1

            # query total data Produk 
            queryProduk = graph.run('MATCH (p:kategori)-[]-(q:UMKM)-[]-(r:produk) WHERE '+str(keyword)+' RETURN p.nama_produk LIMIT 50').data()
            counterProduk = 0
            for value in queryProduk:
                counterProduk += 1

            callback = {"status": "success", "kategori": counterKategori, "umkm": counterUmkm, "produk": counterProduk}
        except:
            callback = {"status": "failed"}
        return jsonify(callback)

# proses untuk menampilkan data selengkapnya
@user_bp.route("/infoLengkap", methods=["GET"])
def infoLengkap():
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        id = request.args.get("id")
        try:
            # query untuk menampilkan data pada modal yang tampil (data pemilik, data umkm, data produk)
            hasil = graph.run('MATCH (p:users)-[]-(q:UMKM)-[]-(k:kategori) WHERE id(q)='+id+' RETURN DISTINCT id(q) as id, p.nama as nama_pemilik, q.nama_usaha as nama_usaha, q.foto as foto, q.portfolio as portfolio, q.thn_berdiri as thn_berdiri, q.latitude as lat, q.longitude as long, q.klasifikasi as klasifikasi, q.legalitas as legalitas, q.kecamatan as kecamatan, q.kelurahan as kelurahan, k.subsektor as subsektor, q.nohp as nohp, q.email as email, q.facebook as facebook, q.instagram as instagram').data()
            callback = {"status": "success", "data": hasil}
        except:
            callback = {"status": "failed"}
        return jsonify(callback)

# menampilkan halaman umkm
@user_bp.route("/umkm")
def umkm():
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        now = datetime.datetime.now()
        # query menampilkan data umkm berdasarkan pengguna yg sedang login
        data_umkm = graph.run("MATCH (p:users)-[]-(u:UMKM) WHERE id(p)="+str(session.get('id'))+" RETURN id(u) as id, u.nama_usaha as nama_usaha, u.thn_berdiri as thn_berdiri, u.foto as foto, u.portfolio as portfolio, u.klasifikasi as klasifikasi, u.legalitas as legalitas, u.kecamatan as kecamatan, u.kelurahan as kelurahan, u.longitude as longitude, u.latitude as latitude, u.nohp as nohp, u.email as email, u.facebook as facebook, u.instagram as instagram").data()
        return render_template(
            "user/umkm.html",
            title = "UMKM",
            app_name =__app_name,
            base_url = base_url,
            year = now.year,
            data_umkm = data_umkm
        )

# proses tambah UMKM
@user_bp.route("/tambah_umkm", methods=["POST"])
def tambah_umkm():
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        nama_usaha = request.form.get("nama_usaha")
        thn_berdiri = __filter_input(request.form.get("thn_berdiri"))
        longitude = request.form.get("longitude")
        latitude = request.form.get("latitude")
        nohp = request.form.get("nohp")
        email = request.form.get("email")
        facebook = request.form.get("facebook")
        instagram = request.form.get("instagram")
        klasifikasi = request.form.get("klasifikasi")
        legalitas = request.form.get("legalitas")
        kecamatan = request.form.get("kecamatan")
        kelurahan = request.form.get("kelurahan")
        subsektor = request.form.get("subsektor")

        # Upload Gambar
        gambar = request.files['foto_usaha']
        if gambar and allowed_image(gambar.filename):
            gambarname = secure_filename(gambar.filename)
            gambar.save(os.path.join(app.config['UPLOAD_FOLDER'], gambarname))
        else:
            flash("Hanya menerima gambar dengan format png, jpg, jpeg", "error")
            return redirect(url_for("user_bp.umkm"))

        # Upload Portfolio
        file = request.files['portfolio']
        if file and allowed_image(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        else:
            flash("Hanya menerima file dengan format png, jpg, jpeg", "error")
            return redirect(url_for("user_bp.umkm"))

        try:
            # query menambahkan data umkm kemudian merelasikan data umkm yg dibuat dengan pengguna yg sedang login
            graph.run("MATCH (p:users) WHERE id(p)="+str(session.get('id'))+" MERGE (k:kategori{subsektor:'"+ subsektor +"'}) CREATE (u:UMKM {nama_usaha: '" + nama_usaha + "', thn_berdiri: '" + thn_berdiri + "', klasifikasi: '" + klasifikasi + "', legalitas: '" + legalitas + "', kecamatan: '" + kecamatan + "', kelurahan: '" + kelurahan + "', longitude: '" + longitude + "', latitude: '" + latitude + "', nohp: '" + nohp + "', email: '" + email + "', facebook: '" + facebook + "', instagram: '" + instagram + "', foto: '" + gambarname + "', portfolio: '" + filename + "'}), (p)-[:Menyediakan]->(u), (k)-[:Memiliki]->(u)")
            flash("Data berhasil ditambahkan.", "success")
            return redirect(url_for("user_bp.umkm"))
        except:
            flash("Data gagal ditambahkan.", "error")
            return redirect(url_for("user_bp.umkm"))

# halaman edit UMKM
@user_bp.route("/edit_umkm/<id>")
def edit_umkm(id):
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        now = datetime.datetime.now()
        # query menampilkan data umkm yg berelasi dengan pengguna yg sedang login
        data_umkm = graph.run("MATCH (p:users)-[]-(u:UMKM) WHERE id(p)="+str(session.get('id'))+" AND id(u)="+str(id)+" RETURN id(u) as id, u.nama_usaha as nama_usaha, u.thn_berdiri as thn_berdiri, u.klasifikasi as klasifikasi, u.legalitas as legalitas, u.kecamatan as kecamatan, u.kelurahan as kelurahan, u.longitude as longitude, u.latitude as latitude, u.nohp as nohp, u.email as email, u.facebook as facebook, u.instagram as instagram").data()
        return render_template(
            "user/umkm.html",
            title = "UMKM",
            app_name =__app_name,
            base_url = base_url,
            year = now.year,
            data_umkm = data_umkm
        )

# proses edit 
@user_bp.route("/proses_edit_umkm", methods=["POST"])
def proses_edit_umkm():
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        id = request.form.get("id")
        nama_usaha = request.form.get("nama_usaha")
        thn_berdiri = __filter_input(request.form.get("thn_berdiri"))
        longitude = request.form.get("longitude")
        latitude = request.form.get("latitude")
        nohp = request.form.get("nohp")
        email = request.form.get("email")
        facebook = request.form.get("facebook")
        instagram = request.form.get("instagram")
        klasifikasi = request.form.get("klasifikasi")
        legalitas = request.form.get("legalitas")
        kecamatan = request.form.get("kecamatan")
        kelurahan = request.form.get("kelurahan")

        # Upload Gambar
        gambar = request.files['foto_usaha']
        if gambar.filename != '':
            if gambar and allowed_image(gambar.filename):
                gambarname = secure_filename(gambar.filename)
                gambar.save(os.path.join(app.config['UPLOAD_FOLDER'], gambarname))
            else:
                flash("Hanya menerima gambar dengan format png, jpg, jpeg", "error")
                return redirect(url_for("user_bp.umkm"))

        # Upload Portfolio
        file = request.files['portfolio']
        if file.filename != '':
            if file and allowed_image(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            else:
                flash("Hanya menerima file dengan format png, jpg, jpeg", "error")
                return redirect(url_for("user_bp.umkm"))

        try:
            # query proses edit data umkm yg berelasi dengan pengguna yg sedang login
            if file.filename != '' and gambar.filename != '':
                graph.run("MATCH (p:users)-[]-(u:UMKM) WHERE id(p)="+str(session.get('id'))+" AND id(u)="+str(id)+" SET u.nama_usaha= '" + nama_usaha + "', u.thn_berdiri= '" + thn_berdiri + "', u.klasifikasi= '" + klasifikasi + "', u.legalitas= '" + legalitas + "', u.kecamatan= '" + kecamatan + "', u.kelurahan= '" + kelurahan + "', u.longitude= '" + longitude + "', u.latitude= '" + latitude + "', u.nohp= '" + nohp + "', u.email= '" + email + "', u.facebook= '" + facebook + "', u.instagram= '" + instagram + "', u.foto= '" + gambarname + "', u.portfolio= '" + filename + "' ")
            elif file.filename != '' and gambar.filename == '':
                graph.run("MATCH (p:users)-[]-(u:UMKM) WHERE id(p)="+str(session.get('id'))+" AND id(u)="+str(id)+" SET u.nama_usaha= '" + nama_usaha + "', u.thn_berdiri= '" + thn_berdiri + "', u.klasifikasi= '" + klasifikasi + "', u.legalitas= '" + legalitas + "', u.kecamatan= '" + kecamatan + "', u.kelurahan= '" + kelurahan + "', u.longitude= '" + longitude + "', u.latitude= '" + latitude + "', u.nohp= '" + nohp + "', u.email= '" + email + "', u.facebook= '" + facebook + "', u.instagram= '" + instagram + "', u.portfolio= '" + filename + "' ")
            elif file.filename == '' and gambar.filename != '':
                graph.run("MATCH (p:users)-[]-(u:UMKM) WHERE id(p)="+str(session.get('id'))+" AND id(u)="+str(id)+" SET u.nama_usaha= '" + nama_usaha + "', u.thn_berdiri= '" + thn_berdiri + "', u.klasifikasi= '" + klasifikasi + "', u.legalitas= '" + legalitas + "', u.kecamatan= '" + kecamatan + "', u.kelurahan= '" + kelurahan + "', u.longitude= '" + longitude + "', u.latitude= '" + latitude + "', u.nohp= '" + nohp + "', u.email= '" + email + "', u.facebook= '" + facebook + "', u.instagram= '" + instagram + "', u.foto= '" + gambarname + "' ")
            else:
                graph.run("MATCH (p:users)-[]-(u:UMKM) WHERE id(p)="+str(session.get('id'))+" AND id(u)="+str(id)+" SET u.nama_usaha= '" + nama_usaha + "', u.thn_berdiri= '" + thn_berdiri + "', u.klasifikasi= '" + klasifikasi + "', u.legalitas= '" + legalitas + "', u.kecamatan= '" + kecamatan + "', u.kelurahan= '" + kelurahan + "', u.longitude= '" + longitude + "', u.latitude= '" + latitude + "', u.nohp= '" + nohp + "', u.email= '" + email + "', u.facebook= '" + facebook + "', u.instagram= '" + instagram + "' ")
            flash("Data berhasil diedit.", "success")
            return redirect(url_for("user_bp.umkm"))
        except:
            flash("Data gagal diedit.", "error")
            return redirect(url_for("user_bp.umkm"))

# proses hapus data umkm
@user_bp.route("/delete_umkm/<id>")
def delete_umkm(id):
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        try:
            # query hapus data umkm yg berelasi dengan pengguna yg sedang login
            graph.run("MATCH (n) WHERE id(n)="+str(id)+" DETACH DELETE n")
            flash("Data berhasil dihapus.", "success")
            return redirect(url_for("user_bp.umkm"))
        except:
            flash("Data gagal dihapus.", "error")
            return redirect(url_for("user_bp.umkm"))

# Halaman Produk
@user_bp.route("/produk")
def produk():
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        now = datetime.datetime.now()
        # query untuk menampilkan data umkm dan data produk yg saling berelasi
        data_umkm = graph.run("MATCH (p:users)-[]-(u:UMKM) WHERE id(p)="+str(session.get('id'))+" RETURN id(u) as id, u.nama_usaha as nama_usaha").data()
        data_produk = graph.run("MATCH (p:users)-[]-(u:UMKM)-[]-(pr:produk) WHERE id(p)="+str(session.get('id'))+" RETURN id(pr) as id, pr.foto as foto, pr.nama_produk as nama_produk, pr.jenis_produk as jenis_produk, pr.deskripsi_produk as deskripsi_produk").data()
        return render_template(
            "user/produk.html",
            title = "Produk",
            app_name =__app_name,
            base_url = base_url,
            year = now.year,
            data_umkm = data_umkm,
            data_produk = data_produk
        )

# proses tambah produk
@user_bp.route("/tambah_produk", methods=["POST"])
def tambah_produk():
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        id_umkm = request.form.get("id_umkm")
        nama_produk = request.form.get("nama_produk")
        jenis_produk = request.form.get("jenis_produk")
        deskripsi_produk = request.form.get("deskripsi_produk")

        # Upload Gambar
        file = request.files['foto_produk']
        if file and allowed_image(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        else:
            flash("Hanya menerima file dengan format png, jpg, jpeg", "error")
            return redirect(url_for("user_bp.produk"))
        try:
            # query proses tambah produk dan merelasikan dengan umkm dari pengguna yg sedang login
            graph.run("MATCH (u:UMKM) WHERE id(u)="+str(id_umkm)+" CREATE (pr:produk {nama_produk: '" + nama_produk + "', jenis_produk: '" + jenis_produk + "', deskripsi_produk: '" + deskripsi_produk + "', foto: '" + filename + "'}), (u)-[:Mempunyai]->(pr)")
            flash("Data berhasil ditambahkan.", "success")
            return redirect(url_for("user_bp.produk"))
        except:
            flash("Data gagal ditambahkan.", "error")
            return redirect(url_for("user_bp.produk"))

# halaman edit produk
@user_bp.route("/edit_produk/<id>")
def edit_produk(id):
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        now = datetime.datetime.now()
        # query menampilkan data umkm dan produk berdasarkan pengguna yg sedang login
        data_umkm = graph.run("MATCH (p:users)-[]-(u:UMKM) WHERE id(p)="+str(session.get('id'))+" RETURN id(u) as id, u.nama_usaha as nama_usaha").data()
        data_produk = graph.run("MATCH (u:UMKM)-[]-(pr:produk) WHERE id(pr)="+str(id)+" RETURN id(u) as id_umkm, u.nama_usaha as nama_usaha, id(pr) as id, pr.nama_produk as nama_produk, pr.jenis_produk as jenis_produk, pr.deskripsi_produk as deskripsi_produk").data()
        return render_template(
            "user/produk.html",
            title = "Produk",
            app_name =__app_name,
            base_url = base_url,
            year = now.year,
            data_umkm = data_umkm,
            data_produk = data_produk
        )

# proses edit produk
@user_bp.route("/proses_edit_produk", methods=["POST"])
def proses_edit_produk():
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        id = request.form.get("id")
        nama_produk = request.form.get("nama_produk")
        jenis_produk = request.form.get("jenis_produk")
        deskripsi_produk = request.form.get("deskripsi_produk")

        # Upload Gambar
        file = request.files['foto_produk']
        if file.filename != '':
            if file and allowed_image(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            else:
                flash("Hanya menerima file dengan format png, jpg, jpeg", "error")
                return redirect(url_for("user_bp.produk"))
        try:
            # query edit produk
            if file.filename != '':
                graph.run("MATCH (p:users)-[]-(u:UMKM)-[]-(pr:produk) WHERE id(p)="+str(session.get('id'))+" AND id(pr)="+str(id)+" SET pr.nama_produk= '" + nama_produk + "', pr.jenis_produk= '" + jenis_produk + "', pr.deskripsi_produk= '" + deskripsi_produk + "', pr.foto= '" + filename + "'")
            else:
                graph.run("MATCH (p:users)-[]-(u:UMKM)-[]-(pr:produk) WHERE id(p)="+str(session.get('id'))+" AND id(pr)="+str(id)+" SET pr.nama_produk= '" + nama_produk + "', pr.jenis_produk= '" + jenis_produk + "', pr.deskripsi_produk= '" + deskripsi_produk + "'")
            flash("Data berhasil diedit.", "success")
            return redirect(url_for("user_bp.produk"))
        except:
            flash("Data gagal diedit.", "error")
            return redirect(url_for("user_bp.produk"))

# proses hapus produk
@user_bp.route("/delete_produk/<id>")
def delete_produk(id):
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        try:
            # query hapus data produk
            graph.run("MATCH (n) WHERE id(n)="+str(id)+" DETACH DELETE n")
            flash("Data berhasil dihapus.", "success")
            return redirect(url_for("user_bp.produk"))
        except:
            flash("Data gagal dihapus.", "error")
            return redirect(url_for("user_bp.produk"))

# Profil user
@user_bp.route("/ubah_profil", methods=["POST"])
def ubah_profil():
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        id = request.form.get("id")
        nama = request.form.get("nama")
        email = request.form.get("email")
        nohp = request.form.get("nohp")
        level = session.get("level")
        try:
            # query update data user yg sedang login
            graph.run("MATCH (p:users) WHERE id(p)="+str(id)+" SET p.nama = '" + nama + "', p.email = '" + email + "', p.nohp = '" + nohp + "'")
            Auth.set_session(id, email, nama, level, nohp)
            flash("Data berhasil diedit.", "success")
            return redirect(url_for("user_bp.dashboard"))
        except:
            flash("Data gagal diedit.", "error")
            return redirect(url_for("user_bp.dashboard"))

# proses ubah password pengguna yg sedang login
@user_bp.route("/ubah_password", methods=["POST"])
def ubah_password():
    if Auth.restrict():
        return redirect(url_for("public_bp.home"))
    else:
        id = request.form.get("id")
        old_password = request.form.get("old_password")
        new_password = request.form.get("new_password")

        # query pengecekan data password 
        get_users = graph.run("MATCH (p:users) WHERE id(p) = " + str(id) + " RETURN n.password").data()

        ph = PasswordHasher(
            memory_cost=65536,
            time_cost=4,
            parallelism=2,
            hash_len=32,
            type=Type.ID
        )

        old_password_hash = get_users[0]["n.password"]
        new_password_hash = ph.hash(new_password)
        if ph.verify(old_password, old_password_hash):
            try:
                # query update data password
                ph.verify(password_hash, password)
                graph.run("MATCH (p:users) WHERE id(p)="+str(id)+" SET p.password = '" + new_password_hash + "'")
                flash("Password berhasil diubah.", "success")
                return redirect(url_for("user_bp.dashboard"))
            except:
                flash("Password gagal diubah.", "error")
                return redirect(url_for("user_bp.dashboard"))
        else:
            flash("Password lama tidak sesuai.", "error")
            return redirect(url_for("user_bp.dashboard"))

def __filter_input(input, mode=True):
    if mode == True:
        input = str(utils.escape(input))

    return input
