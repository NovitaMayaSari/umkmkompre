from flask import Blueprint, render_template, request, redirect, url_for, jsonify, abort
from jinja2 import utils
from auth.check import Auth
from py2neo import Graph
from dotenv import load_dotenv
import datetime
import os

# Take environment variables from .env.
load_dotenv()

# inisiasi nama blueprint, peletakan folder template dan peletakan folderstatic
# template= file html
# statisc= file css dan js
public_bp = Blueprint('public_bp', __name__, template_folder='templates', static_folder='static')

# mengambil data url, user, pass dari file .env
neo4j_url = os.environ.get("NEO4J_URL_HTTPS")
neo4j_user = os.environ.get("NEO4J_USER")
neo4j_pass = os.environ.get("NEO4J_PASSWORD")

#mengkoneksikan ke aura
graph = Graph(neo4j_url, user=neo4j_user, password=neo4j_pass)

# mengambil data app name, base url dari file.env
__app_name = os.environ.get("APP_NAME")
base_url = os.environ.get("BASE_URL")

# Defining the url root of our site
@public_bp.route("/")
def index():
    return redirect(url_for('public_bp.home'))

# menampilkan halaman home
@public_bp.route("/home")
def home():
    if Auth.check_auth():
        return Auth.check_level()
    else:
        now = datetime.datetime.now()
        dataUMKM = graph.query("MATCH (n:users)-[]-(u:UMKM)-[]-(p:produk) WHERE n.level<>'admin' RETURN n.nama as nama_pemilik, u.nama_usaha as nama_usaha, u.latitude as lat, u.longitude as long").data()
        return render_template(
            "public/home.html",
            title= 'Home',
            base_url= base_url,
            app_name=__app_name,
            year=now.year,
            dataUMKM = dataUMKM
        )

# menampilkan halaman pencarian peta
@public_bp.route("/search", methods=["GET"])
def pencarian():
    # mengambil data yang dikirim melalui request dengan methode get dan dengan nama parameter keyword
    keyword = request.args.get("keyword")
    try:
        hasil = graph.run('MATCH (p:kategori)-[*1..2]-(q:UMKM)-[*1..2]-(r:produk) WHERE '+str(keyword)+' RETURN id(q) as id, q.nama_usaha as nama_usaha, q.latitude as lat, q.longitude as long LIMIT 50').data()
        callback = {"status": "success", "data": hasil}
    except:
        callback = {"status": "failed"}
    return jsonify(callback)

def __filter_input(input, mode=True):
    if mode == True:
        input = str(utils.escape(input))
    return input