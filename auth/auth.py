from flask import Blueprint, render_template, request, session, flash, redirect, url_for
from py2neo import Graph
from auth.check import Auth
from argon2 import PasswordHasher, Type
from jinja2 import utils
from dotenv import load_dotenv
import os


# Take environment variables from .env
load_dotenv()

# inisiasi nama blueprint, peletakan folder template dan peletakan folderstatic
# template= file html
# statisc= file css dan js
auth_bp = Blueprint("auth_bp", __name__, template_folder="templates", static_folder="static")

# Set up authentication parameters
# mengambil data url, user, pass dari file .env
neo4j_url = os.environ.get("NEO4J_URL_HTTPS")
neo4j_user = os.environ.get("NEO4J_USER")
neo4j_pass = os.environ.get("NEO4J_PASSWORD")

#mengkoneksikan ke aura
graph = Graph(neo4j_url, user=neo4j_user, password=neo4j_pass)

# mengambil data app name, base url dari file.env
__app_name = os.environ.get("APP_NAME")
base_url = os.environ.get("BASE_URL")

# Untuk halaman login admin dan user
@auth_bp.route("/masuk")
def login():
    if Auth.check_auth():
        return Auth.check_level()
    else:
        return render_template(
            "auth/masuk.html",
            title="Masuk",
            base_url= base_url,
            app_name=__app_name
        )

# proses pengecekan login
@auth_bp.route("/masuk/check", methods=["POST"])
def login_check():
    # mengambil data menggunakan methode post dari submit login
    email = __filter_input(request.form.get("email"))
    password = request.form.get("password")

    # melakukan pengecekan email di database 
    get_users = graph.run("MATCH (n:users) WHERE n.email = '" + email + "' RETURN ID(n) as id, n.email, n.password, n.nama, n.level, n.nohp").data()

    # pengecekan jumlah data yang ditemukan
    if len(get_users) > 0:
        ph = PasswordHasher(
            memory_cost=65536,
            time_cost=4,
            parallelism=2,
            hash_len=32,
            type=Type.ID
        )

        # mengambil data password dari database
        password_hash = get_users[0]["n.password"]

        try:
            # memverifikasi password apakah sudah benar dngn database
            ph.verify(password_hash, password)

            # menyimpan data user dari database kedalam variabel
            id = get_users[0]["id"]
            email = get_users[0]["n.email"]
            nama = get_users[0]["n.nama"]
            level = get_users[0]["n.level"]
            nohp = get_users[0]["n.nohp"]

            # pengecekan level
            if level == "admin":
                Auth.set_session(id, email, nama, level, nohp)
                return redirect(url_for("admin_bp.dashboard"))
            elif level == "user":
                Auth.set_session(id, email, nama, level, nohp)
                return redirect(url_for("user_bp.dashboard"))
            else:
                flash(
                    "Maaf, Anda tidak punya hak akses.", "error")

                return redirect(url_for("auth_bp.login"))
        except:
            flash(
                "Password yang Anda masukkan salah, silahkan coba lagi.", "error")

            return redirect(url_for("auth_bp.login"))
    else:
        flash(
            "Email yang Anda masukkan salah, silahkan coba lagi.", "error")
        return redirect(url_for("auth_bp.login"))

# menampilkan form pendaftaran
@auth_bp.route("/daftar")
def register():
    if Auth.check_auth():
        return Auth.check_level()
    else:
        return render_template(
            "auth/daftar.html",
            title="Pendaftaran",
            base_url= base_url,
            app_name=__app_name
        )

# proses pendaftaran
@auth_bp.route("/daftar/check", methods=["POST"])
def register_check():
    # mengambil data menggunakan methode post dari request 
    nama = __filter_input(request.form.get("nama"))
    nohp = __filter_input(request.form.get("nohp"))
    email = __filter_input(request.form.get("email"))
    password = request.form.get("password")

    # melakukan pengecekan email di database apakah sudah terdaftar atau belum
    get_users = graph.run("MATCH (n:users) WHERE n.email = '" + email + "' RETURN n.email").data()

    # jika >0 maka email sudah terdaftar
    if len(get_users) > 0:
        flash(
            "Email yang Anda masukkan sudah digunakan, silahkan coba yang lain.", "error")
        return redirect(url_for("auth_bp.register"))
    # jika email belum terdaftar
    else:
        ph = PasswordHasher(
            memory_cost=65536,
            time_cost=4,
            parallelism=2,
            hash_len=32,
            type=Type.ID
        )

        password_hash = ph.hash(password)

        try:
            graph.run("CREATE (n:users {email: '" + email + "', nohp: '" + nohp + "', password: '" + password_hash + "', nama: '" + nama + "', level: 'user'})")
            flash("Akun berhasil dibuat, silahkan login.", "success")
            return redirect(url_for("auth_bp.login"))
        except:
            flash("Terjadi kesalahan dalam aplikasi, silahkan coba lagi nanti.", "error")
            return redirect(url_for("auth_bp.register"))

# proses log out
@auth_bp.route("/logout", methods=["GET"])
def logout():
    session.clear()
    return redirect(url_for("public_bp.home"))


def __filter_input(input, mode=True):
    if mode == True:
        input = str(utils.escape(input))

    return input