from flask import session, redirect, url_for

# This class was made for Authentication purposes
class Auth():
    # Check if user is logged in or not
    def check_auth():
        if session.get("logged_in") == True:
            return True
        else:
            return False

    # Check level for each user who was registered
    def check_level():
        if session.get("level") == "admin":
            return redirect(url_for("admin_bp.dashboard"))
        elif session.get("level") == "user":
            return redirect(url_for("user_bp.dashboard"))
        else:
            return redirect(url_for("auth_bp.login"))

    # pembatasan akses jika pengguna tidak login 
    def restrict():
        if session.get("logged_in") != True:
            return True
        else:
            return False
            
    # mengatur session ketika berhasil login
    def set_session(id, email, nama, level, nohp):
        session["id"] = id
        session["logged_in"] = True
        session["email"] = email
        session["nama"] = nama
        session["level"] = level
        session["nohp"] = nohp
